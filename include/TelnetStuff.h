// source: https://www.megunolink.com/articles/wireless/talk-esp32-over-wifi/

#ifndef TELNETSTUFF_H
#define TELNETSTUFF_H

// extern const uint ServerPort = 23;
// extern WiFiServer telnetServer(ServerPort);
// extern WiFiClient telnetClient;
// extern uint8_t ReceiveBuffer[32];

void CheckForConnections();
void telnetSend(char* data);
uint8_t* checkTelnetInput();
bool containsNewline(char val, uint8_t* arr);

#endif //TELNETSTUFF_H