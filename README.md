# Setting up the Raspi4
Since ROS runs well on Ubuntu, and Ubuntu 20.04 is released explicitly for Raspi, I decided to go straight for Ubuntu, following these instructions: https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi#1-overview - this worked well, I did it completely headless (only power to the raspi), but had to edit the wifi SSID+pass in the cloud-init-50.yaml after the first boot before I was able to SSH to it.
First step: Install the dual-8MP stereo Arducam board: https://www.arducam.com/docs/cameras-for-raspberry-pi/synchronized-stereo-camera-hat/camarray-arducam-8mp-synchronized-stereo-camera-bundle-kit/

## First hurdle: raspi-config not available
Found help here: https://askubuntu.com/questions/1273700/enable-spi-and-i2c-on-ubuntu-20-04-raspberry-pi
* sudo dpkg -i raspi-config_20220112_all.deb
* sudo apt install lua5.1 alsa-utils
* apt --fix-broken install
* sudo dpkg -i raspi-config_20220112_all.deb - worked this time, now I can run "sudo raspi-config"

So now I went to enable the camera and found it labelled "legacy" and was presented with a message "deprecated", so googled some more and found that now libcamera is advised over the "old" legacy camera driver. Not sure yet how to get that working in Ubuntu 64bit and also unsure if all the stuff I want to do with the camera (stereovision with my Arducam kit, depth mapping, object recognition with tensorflow...) will work this way.

First thing I found out: The raspi-config installed above apparently does not edit the active config files (/boot/firmware/config.txt and usercfg.txt) but rather creates a new config.txt unser /boot which is not used.

I added the following content to usercfg.txt and rebooted:
start_x=1
dtoverlay=vc4-fkms-v3d
gpu_mem=128

still no /dev/video0 though...

I decided to try following these instructions first: https://www.arducam.com/docs/cameras-for-raspberry-pi/synchronized-stereo-camera-hat/camarray-arducam-8mp-synchronized-stereo-camera-bundle-kit/

sudo apt install libraspberrypi-bin --> installs raspistill+raspivid
but still no /dev/video0

## Second try with RaspiOS 64bit
After getting nowhere for 2 days with Ubuntu Server 20.04, I gave up and installed RaspiOS Lite 64bit instead: 
https://downloads.raspberrypi.org/raspios_lite_arm64/images/raspios_lite_arm64-2021-11-08/

Only now to find out that raspistill and raspivid are not supported in 64bit (I want 64bit since my Pi4 has 8GB), I need to use libcamera, which in turn is not mentioned on the Arducam pages.

So far I at least get:
```
$ vcgencmd get_camera
supported=1 detected=0
```
And /dev/video0 exists, which I never succeeded in getting with Ubuntu.

libcamera-hello reports:
```
[...]
[0:10:42.385084300] [1434]  INFO RPI raspberrypi.cpp:620 Sensor: /base/soc/i2c0mux/i2c@1/imx219@10 - Selected mode: 1640x1232-pRAA
[0:10:42.432374126] [1434]  INFO RPISTREAM rpi_stream.cpp:122 No buffers available for ISP Output0
[0:10:42.432417145] [1434]  INFO RPISTREAM rpi_stream.cpp:122 No buffers available for ISP Output0
[0:10:42.432436201] [1434]  INFO RPISTREAM rpi_stream.cpp:122 No buffers available for ISP Output0
[0:10:42.480911451] [1434] ERROR V4L2 v4l2_videodevice.cpp:1766 /dev/video14[16:cap]: Failed to start streaming: No such file or directory
```
I currently have one of the 2 ports of the stereo-camera-module plugged directly into the Raspi camera port, not sure if that should work or not, but at least something is detected, and if I disconnect the camera (or put the ribbon in the wrong way) then I get `ERROR: *** no cameras available ***` so at least something seems to get detected.

FINALLY I got a camera image! Using `libcamera-jpeg -o test.jpg` (takes about 5 seconds for one snapshot) I was able to snap a jpg. For this I had to connect the stereocam board and found that I had misconnected the ribbons between Arducam board and the stereo camera. I find it crazy that apparently some of the onboard ribbon connectors expect the contacts down (towards the PCB, like on the Arducam board) and others (looking exactly alike) expect the contacts upwards, away from the PCB (on the stereo camera board).

But alas, when trying to follow the instructions on https://www.arducam.com/docs/cameras-for-raspberry-pi/synchronized-stereo-camera-hat/opencv-and-depth-map-on-arducam-stereo-camera-hat-tutorial:

```
pi@roverpi:~ $ sudo apt-get install libhdf5-dev libhdf5-serial-dev libatlas-base-dev libjasper-dev libqtgui4 libqt4-test
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Note, selecting 'libhdf5-dev' instead of 'libhdf5-serial-dev'
Package libqtgui4 is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Unable to locate package libjasper-dev
E: Package 'libqtgui4' has no installation candidate
E: Unable to locate package libqt4-test
```
Sigh. Google didnt help here either. So after reading some more, I decided to try the standard PiOS 32bit (with desktop this time, although I never plan to plugin a screen to the Pi) since I guess the extra effort of using 64bit is not worth it. And each process can use upto 3GB of my 8GB even with 32bit OS.

## Third try with RaspiOS 32bit
This time I have trouble with headless install, the procedure I used on my first 2 tries (create "ssh" and "wpa_supplicant" in /boot ) are not working, even with the instructions on https://www.raspberrypi.com/documentation/computers/configuration.html#setting-up-a-headless-raspberry-pi

After wiping the tfcard and reflashing again, and using the long version of wpa_supplicant.conf, now it connects to wifi.

Next problem: Right out of the box, apt seems to be broken:
```
pi@roverpi:~ $ sudo apt-get update
Hit:1 http://raspbian.raspberrypi.org/raspbian bullseye InRelease
Hit:2 http://archive.raspberrypi.org/debian bullseye InRelease
Reading package lists... Done
pi@roverpi:~ $ sudo apt-get upgrade
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Calculating upgrade... Done
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 vlc-bin : Depends: libvlc-bin (= 3.0.16-1+rpi1+rpt1) but 3.0.16-1+rpi1+rpt2 is to be installed
 vlc-plugin-skins2 : Depends: vlc-plugin-qt (= 3.0.16-1+rpi1+rpt2) but 3.0.16-1+rpi1+rpt1 is to be installed
E: Broken packages
```
Following the hint here (https://forums.raspberrypi.com/viewtopic.php?t=323661) "apt upgrade" works better than "apt-get upgrade"

Once that is done running, I hope to fix the complaints about "missing locale" with: `sudo dpkg-reconfigure locales`

Now trying to get a snapshot from the cameras... and only now found out here (https://forums.raspberrypi.com/viewtopic.php?t=323390) that even in the recommended latest RaspiOS 32bit (11=Bullseye) version, libcamera should be used instead of the legacy camera support. Right now I have "camera_auto_detect=1" in my config.txt and commented out "start_x=1". I have a /dev/video0. But libcamera-hello reports:
```
pi@roverpi:~ $ libcamera-hello 
libEGL warning: DRI3: failed to query the version
libEGL warning: DRI2: failed to authenticate
X Error of failed request:  BadRequest (invalid request code or no such operation)
  Major opcode of failed request:  155 ()
  Minor opcode of failed request:  1
  Serial number of failed request:  16
  Current serial number in output stream:  16
```
Solution: The preview window would not open (although I'm using X forwarding via ssh), maybe because the photo/video preview uses an overlay which doesnt work via X fowarding... Now installing VNC server+client.

After installing realvnc server on the PI and realvnc viewer (no other VNC viewer works because the realvnc-server uses linix account authentication) and configuring the Raspi to boot into desktop (logged in with pi user), I am able to VNC to the PI and view camera snapshots with i.e. libcamera-hello.

So now I finally want to continue with the Arducam depthmapping tutorial, but:
```
pi@roverpi:~ $ sudo apt install libhdf5-dev libhdf5-serial-dev libatlas-base-dev libjasper-dev libqtgui4 libqt4-test
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Note, selecting 'libhdf5-dev' instead of 'libhdf5-serial-dev'
Package libqtgui4 is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'libqtgui4' has no installation candidate
E: Unable to locate package libqt4-test
```
Which is the same I encountered above with the 64bit OS. Please God don't tell me that 64bit wasnt the problem at all and I just wasted another day switching to a new OS??

* Turns out I cound install libqtgui4 and libqt4-test instead. At least the libjasper-dev wasn't an issue.
* opencv-python==3.4.6.27 is also not available, installed opencv-python-4.5.5.62 instead.
* stereovision-1.0.4 and matplotlib-3.5.1 worked too (which also installed cycler-0.11.0 fonttools-4.28.5 kiwisolver-1.3.2 matplotlib-3.5.1 packaging-21.3 pyparsing-3.0.6 python-dateutil-2.8.2)
* but finally, running python3 1_test.py: Load libarducam_mipicamera fail.
* found support here: https://forum.arducam.com/t/installation-problems-with-arducam-8mp-syncro-stereo-camera-bundle-kit-bo195s8mp/1305 and after I did the "make install" and ran the 1_test.py again, I get: `RuntimeError: module compiled against API version 0xe but this version of numpy is 0xd`
* pip3 install --upgrade numpy --> upgraded from 1.19.5 to numpy-1.22.1

New problem:
```
pi@roverpi:~/MIPI_Camera/RPI/stereo_depth_demo $ python3 1_test.py
Open camera...
Found sensor imx219 at address 10
mmal: mmal_vc_component_create: failed to create component 'vc.ril.rawcam' (5:ENOENT)
mmal: mmal_component_create_core: could not create component 'vc.ril.rawcam' (5)
mmal: Failed to create rawcam
init_camera: Unexpected result.
```
After reading https://github.com/silvanmelchior/RPi_Cam_Web_Interface/issues/643 I enabled "legacy camera support" in raspi-config, confirmed that that removed camera_auto_detect=1 and inserted start_x=1 in the config.txt. After reboot, the 1_test.py works!!! And even opens an X preview window through the Xforwarding :-)

"vcgencmd get_camera" now reports "supported=1 detected=1"

But: a) VNC stopped working, b) booting now takes 2 minutes instead of 30 seconds, c) the 1_test.py output is very dark, can hardly see anything

apt install libcanberra-gtk3-module -> at least that fixed the error "Failed to load module canberra-gtk-module"

Just found this on https://github.com/ArduCAM/MIPI_Camera/blob/master/RPI/README.md:
wget https://project-downloads.drogon.net/wiringpi-latest.deb
sudo dpkg -i wiringpi-latest.deb

Trying to solve the opencv-not-found problem:
https://stackoverflow.com/questions/15320267/package-opencv-was-not-found-in-the-pkg-config-search-path
may now also need: https://github.com/opencv/opencv_contrib

add `pkg-config opencv4 --cflags --libs` to the g++ line in makefile of MIPI (read this tip here: https://stackoverflow.com/questions/24337932/cannot-get-opencv-to-compile-because-of-undefined-references)
NOTE: The opencv-libs must be included at the END of the commandline - otherwise some of the compiles will still fail, even though included in the commandline before the sourcefile

Thanks to http://www.netzmafia.de/skripten/hardware/RasPi/RasPi_I2C.html I am now able to see that the stereoHAT is on I2C Bus #10 and can now be controlled by i2cset.

Finally fixed VNC issue by setting hdmi_force_hotplug=1 in /boot/config.txt (for now...)

Thanks to https://www.hackster.io/sridhar-rajagopal/raspberry-pi-high-quality-camera-headless-setup-tips-37903c I can also now see all camera previews in VNC: Select VNC -> Hamburger Menu -> Options, Select Troubleshooting on the left. Make sure "Enable direct capture mode" is checked.

Fantastic ALSA/USB audio instructions here: https://raspberrypi.stackexchange.com/questions/80072/how-can-i-use-an-external-usb-sound-card-and-set-it-as-default

Some notes on using triggered OV9281 cameras: https://forums.raspberrypi.com/viewtopic.php?t=293959

Random Notes: Stereovision cameras need to be synchronized (when moving) or the 2 frames will be taken at slightly different times = locations and will confuse the stereoscopic vision. Likewise when using the camera vision for orientation/mapping (SLAM) i.e. ORB-Slam or DSO-Slam etc. the cameras should use a global shutter (not a rolling one) so all the pixels in the complete frame are exposed at exactly the same time, otherwise, again, there will be differences dur to the motion. For detecting edges, like for DSO-Slam, monochrome (black&white) cameras are better because of better contrast and light sensitivity.

Some cameras can apparently be modified to sync each other: https://www.arducam.com/docs/cameras-for-raspberry-pi/synchronized-stereo-camera-hat/sync-stereo-camera-hat-user-manual/

Also, I'm currently not sure if I may have an issue with Depth-of-Field (Schärfentiefe), since lenses are usually focussed to a certain distance from the lens, and anything too far away (either nearer or closer) from that focussed dictance will become blurry. So ideally I need a pinhole camera which is perfectly focussed everywhere.

https://vision.in.tum.de/research/vslam

### USB cameras
I found a cheap USB OV9281 on Amazon and decided to give it a try while I was waiting for the 2 CSI versions to arrive, which I also ordered.

My first stop was here: https://raspberrypi-guide.github.io/electronics/using-usb-webcams
```
pi@roverpi:~ $ lsusb
Bus 002 Device 001: ID 1d6b:0003 Linux Foundation 3.0 root hub
Bus 001 Device 003: ID 0bda:5846 Realtek Semiconductor Corp. HBVCAM Camera
Bus 001 Device 002: ID 2109:3431 VIA Labs, Inc. Hub
Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
pi@roverpi:~ $ sudo apt install fswebcam

dmesg shows:
[94791.790532] usb 1-1.3: new high-speed USB device number 3 using xhci_hcd
[94791.959136] usb 1-1.3: New USB device found, idVendor=0bda, idProduct=5846, bcdDevice= 0.07
[94791.959158] usb 1-1.3: New USB device strings: Mfr=3, Product=1, SerialNumber=2
[94791.959176] usb 1-1.3: Product: HBVCAM Camera
[94791.959193] usb 1-1.3: Manufacturer: Generic
[94791.959210] usb 1-1.3: SerialNumber: 200901010001
[94792.080301] uvcvideo: Found UVC 1.00 device HBVCAM Camera (0bda:5846)
[94792.100862] uvcvideo: Failed to initialize entity for entity 6
[94792.100871] uvcvideo: Failed to register entities (-22).
[94792.101016] input: HBVCAM Camera: HBVCAM Camera as /devices/platform/scb/fd500000.pcie/pci0000:00/0000:00:00.0/0000:01:00.0/usb1/1-1/1-1.3/1-1.3:1.0/input/input6
[94792.101158] usbcore: registered new interface driver uvcvideo
[94792.101164] USB Video Class driver (1.1.1)

pi@roverpi:~ $ ll /dev/video*
crw-rw----+ 1 root video 81,  8 Jan 23 15:10 /dev/video0
crw-rw----+ 1 root video 81,  9 Jan 23 15:10 /dev/video1
crw-rw----+ 1 root video 81,  0 Jan 23 15:10 /dev/video10
crw-rw----+ 1 root video 81,  1 Jan 23 15:10 /dev/video11
crw-rw----+ 1 root video 81,  4 Jan 23 15:10 /dev/video12
crw-rw----+ 1 root video 81,  2 Jan 23 15:10 /dev/video13
crw-rw----+ 1 root video 81,  3 Jan 23 15:10 /dev/video14
crw-rw----+ 1 root video 81,  5 Jan 23 15:10 /dev/video15
crw-rw----+ 1 root video 81,  6 Jan 23 15:10 /dev/video16
crw-rw----+ 1 root video 81,  7 Jan 23 15:10 /dev/video18
crw-rw----+ 1 root video 81, 11 Jan 24 17:31 /dev/video2
crw-rw----+ 1 root video 81, 12 Jan 24 17:31 /dev/video3
(Note that video1,video2,video3 are all new and caused by plugging in the USB camera)

After powering off, unplugging the CSI camera (which wasnt working right anyway) and powering back on, I could use /dev/video0:

pi@roverpi:~ $ fswebcam -r 640x400 --no-banner /tmp/image1.jpg -d /dev/video0
--- Opening /dev/video0...
Trying source module v4l2...
/dev/video0 opened.
No input was specified, using the first.
Adjusting resolution from 640x400 to 640x480.
--- Capturing frame...
Captured frame in 0.00 seconds.
--- Processing captured image...
Disabling banner.
Writing JPEG image to '/tmp/image1.jpg'.
```
I don't like the "adjusting resolution" though and the image is pretty dark, but it's a first step :-)

```
pi@roverpi:~ $ fswebcam -d /dev/video0 --list-controls
--- Opening /dev/video0...
Trying source module v4l2...
/dev/video0 opened.
No input was specified, using the first.
Available Controls        Current Value   Range
------------------        -------------   -----
Brightness                128 (50%)       0 - 255
Contrast                  32 (12%)        0 - 255
Saturation                0 (0%)          0 - 100
Hue                       0 (50%)         -180 - 180
White Balance Temperature, Auto True            True | False
Gamma                     120 (80%)       0 - 150
Power Line Frequency      50 Hz           Disabled | 50 Hz | 60 Hz
White Balance Temperature 4600 (48%)      2800 - 6500
Sharpness                 0               0 - 7
Backlight Compensation    1               0 - 2
Exposure, Auto            Aperture Priority Mode Manual Mode | Aperture Priority Mode
Exposure (Absolute)       156 (1%)        2 - 10000
Exposure, Auto Priority   False           True | False
Pan (Absolute)            0 (50%)         -57600 - 57600
Tilt (Absolute)           0 (50%)         -57600 - 57600
Focus (absolute)          68 (6%)         0 - 1023
Focus, Auto               True            True | False
Zoom, Absolute            0               0 - 3
Adjusting resolution from 384x288 to 640x480.
--- Capturing frame...
Captured frame in 0.00 seconds.
--- Processing captured image...
There are unsaved changes to the image.
```

## Fresh restart with Ubuntu 20.04 x64
So after realizing that I wasn't going to get a visual SLAM node up-and-running without a lot of tinkering anyway, I decided to give Ubuntu another go, since I will need to compile a lot of tools/libraries anyway.

I bought 2 new monochrome CSI cameras (OV9281 from inno-maker, 30€ each on Amazon) since I want global shutter cameras with high sensitivity and framerate. I'm aware that they may not be 100% compatible with the Arducam versions, but at half the price, I want to give it a try. At least I hope I can get them synchronized and working with the Arducam stereo camera hat, but for now I'm going for monocular ORB-SLAM2 to begin with.

I also bought a secondhand "mini gaming pc" (i7, 16GB RAM, 128GB SSD, GTX 1060) for 400€ on Ebay so i would have at least one machine with enough power for SLAM, in case I don't get anywhere with the Pi4. My own desktop PC has no CUDA-capable GPU either so I need one anyway, and I can always use it for gaming later ;-)

It seems that many ORB-SLAM projects and videos online simply use the test data/videostreams from https://vision.in.tum.de/data/datasets/rgbd-dataset , I havent found much projects/videos describing in detail what cameras, focal length, recording, and processing hardware (CPU, GPU, memory, load, etc.) they used. Apparently a Raspi4 alone is not fast enough. But I havent found a source yet that has actually tried it.

So, step-by-step:
* https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi (with Ubuntu Server x64, headless)
* apt update, upgrade (takes quite a while, at first unattended-upgrade is running and blocks everything else while updating about 95 packages in the background)
* change hostname, create pi user, add ssh keys
* sudo apt install libraspberrypi-bin
* added dtoverlay=ov9281 to /boot/firmware/usercfg.txt
* Considering using docker while still experimenting, so I don't mess up my system too much while trying out 100's of different packages/libraries, different Python versions, etc. trying to get something running

Some interesting instructions here: https://mfurkannargul.medium.com/install-ubuntu-18-04-77a003f392f7
Not sure if that will work with Ubuntu 20 64-bit though - I'll soon find out ;-)

https://medium.com/@gibryonbhojraj/how-to-raspberry-pi-64-bit-with-camera-support-def95f206188 handles Arch Linux, but hopefully will help

ubuntu@roverpi:~$ vcgencmd get_camera
supported=0 detected=0

debugging with help of: https://forums.raspberrypi.com/viewtopic.php?t=313531

```
ubuntu@roverpi:~$ sudo i2cdetect -y 10
     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f
00:          -- -- -- -- -- -- -- -- -- -- -- -- -- 
10: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
20: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
30: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
40: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
50: -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- 
60: UU -- -- -- 64 -- -- -- -- -- -- -- -- -- -- -- 
70: 70 -- -- -- -- -- -- --             

ubuntu@roverpi:~$ sudo i2cdetect -l
i2c-10	i2c       	i2c-11-mux (chan_id 1)          	I2C adapter
i2c-1	  i2c       	bcm2835 (i2c@7e804000)          	I2C adapter
i2c-11	i2c       	bcm2835 (i2c@7e205000)          	I2C adapter
i2c-0	  i2c       	i2c-11-mux (chan_id 0)          	I2C adapter

ubuntu@roverpi:~$ ll /dev/video*
crw-rw---- 1 root video 81, 0 Jan 10 04:56 /dev/video0
crw-rw---- 1 root video 81, 1 Jan 10 04:56 /dev/video1

installed Python is 3.8.10

```

Apparently there are no pre-built libcamera packages for Ubuntu 64-bit on Arm. So compiling myself: https://encomhat.com/2021/05/install-libcamera-raspi/

git clone git://linuxtv.org/libcamera.git and install meson, g++, libgnutls28-dev, libboost-dev, python3-yaml python3-ply # TODO: Why not use official libcamera.org git?
Get:1 http://ports.ubuntu.com/ubuntu-ports focal/universe arm64 ninja-build arm64 1.10.0-1build1 [101 kB]
Get:2 http://ports.ubuntu.com/ubuntu-ports focal/universe arm64 meson all 0.53.2-2ubuntu2 [376 kB]

now this works: ~/libcamera$ meson build #TODO: try adding  -D v4l2=true
after finishing the instructions on the above linked page, I'm stuck with no libcamera-apps and "vcgencmd get_camera" saying "supported=0 detected=0"

https://www.raspberrypi.com/documentation/accessories/camera.html#libcamera-and-libcamera-apps-packages this implies that apt should find an libcamera-apps, but mine doesnt.. so following the manual build instructions:
* git clone https://github.com/raspberrypi/libcamera-apps.git #TODO any difference from libcamera.org git?
* sudo apt install -y cmake libboost-program-options-dev libdrm-dev libexif-dev pkg-config
* (TODO: Add openCV and tflite later)
* sudo apt install libjpeg-dev libpng-dev libtiff-dev
* make -j4
* sudo make install
* sudo ldconfig
but:
```
ubuntu@roverpi:~/build/libcamera-apps/build$ libcamera-hello
Preview window unavailable
[0:42:53.493188302] [4850]  INFO Camera camera_manager.cpp:293 libcamera v0.0.0+3404-7ea52d2b
[0:42:53.498709864] [4851] ERROR RPI dma_heaps.cpp:53 Could not open any dmaHeap device
[0:42:53.499465611] [4851] ERROR RPI raspberrypi.cpp:1138 Failed to register camera mov9281 10-0060: -12
ERROR: *** no cameras available ***
```
found interesting command: vcgencmd get_config arm_freq (see https://elinux.org/RPiconfig)

now in usercfg.txt:
camera_auto_detect=1
dtoverlay=ov9281
gpu_mem=256
hdmi_force_hotplug=1
arm_boost=1

still: vcgencmd get_camera --> supported=0 detected=0

found a clue here: https://www.arducam.com/docs/cameras-for-raspberry-pi/migrated/ov9281-1mp-global-shutter-raspberrypi-camera/

ubuntu@roverpi:~$ /boot/overlays/ov9281.dtbo
-bash: /boot/overlays/ov9281.dtbo: No such file or directory

ubuntu@roverpi:~$ ll /boot/firmware/overlays/ov9281.dtbo
-rwxr-xr-x 1 root root 2981 Jan 27 13:26 /boot/firmware/overlays/ov9281.dtbo*

sudo apt install v4l-utils
v4l2-ctl --stream-mmap --stream-count=-1 -d /dev/video0 --stream-to=/dev/null
<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< 120.63 fps
Yessssss so the camera is at least detected by v4l2 :-)

Finally some success:
```
ubuntu@roverpi:~$ v4l2-ctl -d /dev/video0 --list-formats-ext
ioctl: VIDIOC_ENUM_FMT
	Type: Video Capture

	[0]: 'Y10P' (10-bit Greyscale (MIPI Packed))
		Size: Discrete 1280x800
	[1]: 'Y10 ' (10-bit Greyscale)
		Size: Discrete 1280x800
	[2]: 'GREY' (8-bit Greyscale)
		Size: Discrete 1280x800
```

This works too:
ubuntu@roverpi:~$ fswebcam -r 640x480 web-cam-shot.jpg
--- Opening /dev/video0...
Trying source module v4l2...
/dev/video0 opened.
No input was specified, using the first.
Adjusting resolution from 640x480 to 1280x800.
--- Capturing frame...
Captured frame in 0.00 seconds.
--- Processing captured image...
Writing JPEG image to 'web-cam-shot.jpg'.

but libcamera-hello still doesnt...

maybe later, I will completely rebuild libcamera + apps following these instructions:
https://www.raspberrypi.com/documentation/accessories/camera.html#building-libcamera-apps

But for now, since fswebcam works, I suspect I don't even need libcamera to publish a live feed to ROS
So next, I will install ROS2 and find out! This looks like a good starting point: http://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Binary.html

(Side note): Developing on a headless Pi4: https://answers.ros.org/question/382835/best-way-to-programm-on-ubuntu-server-ros2-foxy-rpi4/

* wget https://github.com/ros2/ros2/releases/download/release-foxy-20211013/ros2-foxy-20211013-linux-focal-arm64.tar.bz2
* ~/ros2_galactic$ tar xf ../ros2-foxy-20211013-linux-focal-arm64.tar.bz2 
* sudo apt update
* sudo apt install python3-rosdep
* sudo rosdep init
* rosdep update

NOTE: In the above steps I just used the sugegsted folder name "galactic", not noticing that I downloaded foxy and should have used that release name. So I renamed the directory at this point and re-ran "rosdep update" and hope it will be ok now.

* rosdep install --from-paths ~/ros2_foxy/ros2-linux/share --rosdistro=foxy --ignore-src -y --skip-keys "cyclonedds fastcdr fastrtps rti-connext-dds-5.3.1 urdfdom_headers"

and now the examples (talker/listener) work! Nice :-)

About Running Rviz in a docker container: https://answers.ros.org/question/301056/ros2-rviz-in-docker-container/

## Rover design

28.01.22 Before I further tackle CV, I want to get my selfmade ESP32-robot working reliably with PID control so it can be remotely steered by console. I have a simple rover built on a 60x60cm board with 2x 250W BLDC hub motors (24v electric scooter wheels), 2x 400W BLDC controllers, an ESP32 for bluetooth remote control via a PS3 controller, and a swivel wheel. The controllers take PWM input and output a squarewave tacho signal at 45 pulses/rotation. The wheels are 200mm in diameter, amounting to ~63cm circumference, so one full pulse (from one rising edge to the next) is 14mm of travel. I already tried 2 weeks ago to implement PID regulation so the rover will reliably move i.e. forwards in a straight line even if the wheels are unevenly loaded (like when one wheel is up against the edge of a carpet) or simply when the motors/controllers don't perfectly keep the same speed all the time when fed with the same PWM signal. But my results were very bad, the PID routine didnt work at all, so I gave up for the moment. Now is the time to catch up on that.

## 2023 Progress
15.01.23 For the past year, I havent had much time to work on this project, so not much has happened. I did build a 80x100cm steel frame, get 2 48V/500W/14.5" geared brushless hub motors (for electric wheelbarrows, 300€ incl. shipping from Aliexpress) and mount them. I dimensioned the steel frame so that a Pylontech US2000 battery unit would nicely fit in between the drive wheels, hopefully providing plenty of energy later on. But the controllers (https://www.ebay.de/itm/275054093098) proved not to work very well with them, the 16A(max20A) are apparently not enough to get the motor started from a standstill, sometimes it works and sometimes not. So for the past week I was looking around for a better BLDC controller which could deliver 50A constant current at upto 60V. And finally stumbled on the open source VESC (https://vesc-project.com/) which looks like exactly what I need. And the best thing is, code and schematics are open source, so I can even build them myself if I want to. For now though I ordered a dual 70A/60V controller on Banggood (https://flipsky.net/products/flipsky-dual-mini-fsesc6-7-pro-140a-base-on-vesc6-6-with-aluminum-anodized-heat-sink) for 225€ incl. VAT and shipping. If it is what it seems to be, it should be the perfect controller for me. It already integrates PID speed control and can be controlled via CAN bus (among other ways) so I should be able to simply set desired wheel speeds with the EPS32 via CAN and let the controller control ramping, constant speed control, etc.

As a side note, I should mention that my current goal is to get a (manually) remote-controlled rover running so I can test if it will mechanically fulfill my needs (pull a device to smooth the sand in a riding arena) and then I can continue with the "intelligence" of automating it.

The most challenging task will probably to implement the CAN communication to the VESC, here is the best resource I've found so far: https://dongilc.gitbook.io/openrobot-inc/tutorials/control-with-can

07.11.23 After yet another long pause, I resume working again. My current main goal is a robot for smoothng a riding arena. So it needs to pull a heavy rake through sand, reliably. My previous model could not do it, the wheels spun and dug into the sand. So I need bigger wheels with more profile.
