#include <Arduino.h>
#include <WiFi.h>
#include "TelnetStuff.h"



void CheckForConnections()
{
  if (telnetServer.hasClient())
  {
    if (telnetClient.connected())
    {
      telnetServer.available().stop(); // reject
    }
    else
    {
      telnetClient = telnetServer.available(); // accept
    }
  }
}

void telnetSend(char* data) {
  if (telnetClient.connected()) {
    telnetClient.printf(data);
  }
}

uint8_t* checkTelnetInput() {
    while (telnetClient.connected() && telnetClient.available())
    {
        telnetClient.read(ReceiveBuffer, sizeof(ReceiveBuffer));
        if (containsNewline('\n',ReceiveBuffer)) {
            return ReceiveBuffer;
        }
    }
    return NULL;
}

bool containsNewline(char val, uint8_t* arr){
    for(uint8_t i = 0; i < sizeof(arr); i++){
        if(arr[i] == val) return true;
    }
    return false;
}