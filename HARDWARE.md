# Thoughts on hardware choices

There are different desired abilities with different hardware requirements.

## Object detection
For object detection, we need AI (i.e. Tensorflow or tfLite) which benefits from a TPU, like the Google Coral EDGE TPU (i.e. USB version, or Coral Dev Board)

## SLAM (Localization and Mapping)
Visual SLAM (via monocular or binocular camera) like ORB-SLAM, DSO-SLAM, etc. benefits from GPU power (openCV) which suggests i.e. an Nvidia Jetson Nano which can run CUDA.

The big advantage of LIDAR (low-cost 2-dimensional like YPLIDAR) is that the mapping needs only little CPU power. But the disadvantages are a) unreliablility in bright daylight and b) only maps at one height

Interesting conversation about ORB-SLAM vs. DSO-SLAM: https://www.researchgate.net/post/ORB-SLAM-vs-DSO-Possible-to-develop-DSO-to-the-same-level

## random Keywords to research
ORBslam2
ORBslam3 https://github.com/UZ-SLAMLab/ORB_SLAM3
Hectorslam
mono-slam
VI-DSO
RTAB
StereoLSD-SLAM
OKVIS
VISLAM
PTAM
openFABmap
bag-of-words


## ORB-SLAM2
https://github.com/raulmur/ORB_SLAM2
https://courses.ece.cornell.edu/ece6930/ECE6930_Spring16_Final_MEng_Reports/SLAM/Real-time%20ROSberryPi%20SLAM%20Robot.pdf

## GPS Receivers
We want centimeter-level accuracy for GNSS navigation.
* https://www.gnss.store/gnss-gps-modules/99-ublox-zed-f9p-rtk-gnss-receiver-board-with-sma-base-or-rover.html#/26-add_antenna_ann_mb-ann_ms

## Productive machinery
One possible brush for a sweeping machine: https://www.berger-maschinen.at/kehrwalzensatz-fuer-kehrmaschinen-je-oe350-x-750mm-kunststoff-gelb/470161s?c=1097

## UWB RTLS
https://how2electronics.com/getting-started-with-esp32-dw1000-uwb-ultra-wideband-module/
https://github.com/jremington/UWB-Indoor-Localization_Arduino
Update Nov 2024: Bought 4 BU01 modules and will be testing 2D ranging soon.

