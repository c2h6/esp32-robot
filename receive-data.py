import socket
HOST = 'esp32-arduino'        
PORT = 23
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
print ('connected to server')

reader = s.makefile("r")

while True:
  l = reader.readline()
  ts,t,a,b,c,d,e,f,g = l.split(',')
  print(ts + ", ticks=" + t + ", cPWM="+ a+ ", tPWM="+ b+ ", SPD="+ c + ", HDG=" + d + ", kP=" + e + ", kI=" + f + ", kD=" + g, end='')

