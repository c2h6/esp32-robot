#include <Arduino.h>
#include <Ps3Controller.h>
#include <WiFi.h>
#include <ESPmDNS.h>
#include <ArduinoOTA.h>
#include <VescUart.h>
#include <Wire.h>
#include <CAN.h>
#include "vesc_can_bus_arduino.h"
#include "config.h"

#define pwmLeftPin 26
#define pwmRightPin 25
#define LED_BUILTIN 2
//#define I2C_SDA 13          // default 21
//#define I2C_SCL 14          // default 22
#define UART_ID_RIGHT 0
#define UART_ID_LEFT 6
#define CAN_ID_RIGHT 66
#define CAN_ID_LEFT 6

volatile uint32_t totalTicks = 0;

void onConnect();
void notify();

int stickX = 0;
int stickY = 0;
int targetPWMLeft = 0;
int targetPWMRight = 0;
uint8_t batteryStatus = 0;
float newRPMleft = 0;
float newRPMright = 0;

char logSendBuffer[128];
const uint ServerPort = 23;
WiFiServer telnetServer(ServerPort);
WiFiClient telnetClient;
String receivedCommand;

void CheckForConnections();
void telnetSend(char* data);
bool checkTelnetInput();
bool containsNewline(char val, uint8_t* arr);
void parseCommand (String data);

VescUart UART;

float Yaw,Roll,Pitch;
const int IMU_ADDR=0x29;
void getIMUeuler();
void setupIMU();
int8_t CalData, sysCalState, gyroCalState, accelCalState, magCalState;

CANV can;

void setup()
{
  Serial2.begin(115200); // RX=GPIO16, TX=17
  UART.setSerialPort(&Serial2);
  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);

  Serial.begin(115200);
  
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  delay(1000);
  char loop = 5;
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.printf("No Wifi found! Waiting %d seconds...\n",loop);
    delay(1000);
    loop--;
    if (!loop) { 
      Serial.println("Aborting Wifi scan and continuing without Wifi.");
      break;
    }
  }
  telnetServer.begin();
  
  uint8_t new_mac[8] = {0x38,0x4F,0xF0,0x78,0x00,0xAE};
  ps3SetBluetoothMacAddress(new_mac);
  Ps3.attach(notify);
  Ps3.attachOnConnect(onConnect);
  Ps3.begin("38:4F:F0:78:00:AE");
  // Serial.println("Ready.");

  ArduinoOTA
    .onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";

      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    })
    .onEnd([]() {
      Serial.println("\nEnd");
    })
    .onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    })
    .onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
  ArduinoOTA.setHostname("esp32rover");
  ArduinoOTA.begin();

  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  setupIMU();

  can.initialize(4,5,250E3);

  Serial.printf((char*)"Setup Routine finished.\n");
}

void loop()
{
  ArduinoOTA.handle();
  CheckForConnections();
  
  if (checkTelnetInput()) {
    parseCommand(receivedCommand);
    receivedCommand = "";
  }
  if (millis() % 500 == 0) { // every 500ms
    
    can.spin();

    if ( UART.getVescValues(UART_ID_LEFT) ) {
      Serial.printf("LEFT newRPM: %6.2f, RPM:%6.2f, Vin:%6.2f, Tfet:%6.2f, Iin:%6.2f, Imot:%6.2f\n",
      newRPMleft,
      //UART.fw_version.major, UART.fw_version.minor,
      UART.data.rpm,
      UART.data.inpVoltage,
      UART.data.tempMosfet,
      UART.data.avgInputCurrent,
      UART.data.avgMotorCurrent);
    }
    // else Serial.println("LEFT VescUart Error: " + UART.data.error);

    if ( UART.getVescValues(UART_ID_RIGHT) ) {
      Serial.printf("RIGHT newRPM: %6.2f, RPM:%6.2f, Vin:%6.2f, Tfet:%6.2f, Iin:%6.2f, Imot:%6.2f\n",
      newRPMright,
      //UART.fw_version.major, UART.fw_version.minor,
      UART.data.rpm,
      UART.data.inpVoltage,
      UART.data.tempMosfet,
      UART.data.avgInputCurrent,
      UART.data.avgMotorCurrent);
    }
    // else Serial.println("Failed to get right VescUart data!");

    Serial.printf("CAN RPM:%6d, Vin:%3.2f, Tfet:%3.1f, Iin:%3.1f, Imot:%3.1f\n",
      can.erpm, can.inpVoltage, can.tempFET, can.avgInputCurrent, can.avgMotorCurrent);

    UART.sendKeepalive(UART_ID_LEFT);
    UART.sendKeepalive(UART_ID_RIGHT);
    
    snprintf(logSendBuffer, 128, "%010lu,%d,%+6.2f,%+6.2f\n",
      millis(), totalTicks, newRPMleft, newRPMright);
    telnetSend(logSendBuffer);

    getIMUeuler();
    Serial.printf("SGAM:%d%d%d%d, Yaw: %+3.2f, Pitch:%+3.2f, Roll:%+3.2f\n",
      sysCalState, gyroCalState, accelCalState, magCalState, Yaw, Pitch, Roll);

    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

}

void notify()
{
  if( abs(Ps3.event.analog_changed.stick.ly)){
    newRPMleft = (float)(-Ps3.data.analog.stick.ly)*50;
    UART.setRPM(newRPMleft, UART_ID_LEFT);
  }
  if( abs(Ps3.event.analog_changed.stick.ry)){
    newRPMright = (float)(-Ps3.data.analog.stick.ry)*50;
    UART.setRPM(newRPMright, UART_ID_RIGHT);
  }

   //---------------------- Battery events ---------------------
    if( batteryStatus != Ps3.data.status.battery ){
        batteryStatus = Ps3.data.status.battery;
        Serial.print("The controller battery is ");
        if( batteryStatus == ps3_status_battery_charging )      Serial.println("charging");
        else if( batteryStatus == ps3_status_battery_full )     Serial.println("FULL");
        else if( batteryStatus == ps3_status_battery_high )     Serial.println("HIGH");
        else if( batteryStatus == ps3_status_battery_low)       Serial.println("LOW");
        else if( batteryStatus == ps3_status_battery_dying )    Serial.println("DYING");
        else if( batteryStatus == ps3_status_battery_shutdown ) Serial.println("SHUTDOWN");
        else Serial.println("UNDEFINED");
    }
}

void onConnect(){
  Serial.println("Connected to PS3 controller!");
}

void CheckForConnections()
{
  if (telnetServer.hasClient())
  {
    if (telnetClient.connected())
    {
      telnetServer.available().stop(); // reject
    }
    else
    {
      telnetClient = telnetServer.available(); // accept
    }
  }
}

void telnetSend(char* data) {
  if (telnetClient.connected()) {
    telnetClient.printf(data);
  }
}

bool checkTelnetInput() {
  while (telnetClient.connected() && telnetClient.available())
  {
    char inChar = (char)telnetClient.read();
    if (inChar != '\n') {
      receivedCommand += inChar;
      Serial.println("Received command:" + receivedCommand);
    } else return true;
  }
  return false;
}

bool containsNewline(char val, char* arr){
    for(uint8_t i = 0; i < sizeof(arr); i++){
        if(arr[i] == val) return true;
    }
    return false;
}

void parseCommand (String data) {
  String cmd = data.substring(0,2);
  float val = data.substring(2,data.length()).toFloat();
  Serial.printf("\n\ncmd:%s, parameter: %2.2f\n", cmd, val);
  if        (cmd == "P=") {
    //kP = val;
  } else if (cmd == "I=") {
    //kI = val;
  } else if (cmd == "D=") {
    //kD = val;
  } else if (cmd == "S=") {
    targetPWMLeft = val;
  }
}

void setupIMU() {
  Wire.begin();
  Wire.setClock(400000); // I2C clock rate ,You can delete it but it helps the speed of I2C (default rate is 100000 Hz)
  delay(100);
  Wire.beginTransmission(IMU_ADDR);
  Wire.write(0x3E); // Power Mode 
  Wire.write(0x00); // Normal:0X00 (or B00), Low Power: 0X01 (or B01) , Suspend Mode: 0X02 (orB10)
  Wire.endTransmission();
  delay(100);
  Wire.beginTransmission(IMU_ADDR);
  Wire.write(0x3D); // Operation Mode
  Wire.write(0x0C); //NDOF:0X0C (or B1100) , IMU:0x08 (or B1000) , NDOF_FMC_OFF: 0x0B (or B1011)
  Wire.endTransmission();
  delay(100);
}

void getIMUeuler() {
  Wire.beginTransmission(IMU_ADDR);

  Wire.write(0x1A);  // get only Euler coords
  Wire.endTransmission(false);
  Wire.requestFrom(IMU_ADDR,6,1);
  // Euler Angles
  Yaw=(int16_t)(Wire.read()|Wire.read()<<8 )/16.00;  //in Degrees unit
  Roll=(int16_t)(Wire.read()|Wire.read()<<8 )/16.00;  //in Degrees unit
  Pitch=(int16_t)(Wire.read()|Wire.read()<<8 )/16.00;  //in Degrees unit

  // Get calibration status
  Wire.write(0x35);  
  Wire.endTransmission(false);
  Wire.requestFrom(IMU_ADDR,1,1);
  CalData=(int8_t)(Wire.read());
  sysCalState = (CalData >> 6) & 0x03;
  gyroCalState = (CalData >> 4) & 0x03;
  accelCalState = (CalData >> 2) & 0x03;
  magCalState = CalData & 0x03;
}